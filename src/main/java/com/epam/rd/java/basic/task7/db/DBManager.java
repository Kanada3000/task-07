package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static final String SELECT_ALL_USERS = "SELECT * FROM users ORDER BY id";
    private static final String SELECT_ALL_TEAMS = "SELECT * FROM teams ORDER BY id";
    private static final String SELECT_USER_BY_LOGIN = "SELECT * FROM users WHERE login=?";
    private static final String SELECT_TEAM_BY_NAME = "SELECT * FROM teams WHERE name=?";
    private static final String SELECT_TEAMS_BY_USER = "SELECT t.* FROM teams t WHERE t.id IN (SELECT team_id FROM users_teams WHERE user_id=?)";
    private static final String INSERT_USER = "INSERT INTO users (login) VALUES (?)";
    private static final String INSERT_TEAM = "INSERT INTO teams (name) VALUES (?)";
    private static final String INSERT_USER_TEAM = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
    private static final String DELETE_USERS_FROM_USER_TEAM = "DELETE FROM users_teams WHERE user_id IN (?)";
    private static final String DELETE_USERS = "DELETE FROM users WHERE id IN (?)";
    private static final String DELETE_TEAM_FROM_USER_TEAM = "DELETE FROM users_teams WHERE team_id IN (?)";
    private static final String DELETE_TEAM = "DELETE FROM teams WHERE id IN (?)";
    private static final String UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";


    private static DBManager instance;

    static {
        instance = new DBManager();
    }

    private Connection getConnection() throws SQLException {
        return getConnection(true);
    }

    private Connection getConnection(boolean autocommit) throws SQLException {
        Properties props = new Properties();

        try (InputStream in = Files.newInputStream(Paths.get("app.properties"))) {
            props.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Connection con = DriverManager.getConnection(props.getProperty("connection.url"));
        con.setAutoCommit(autocommit);
        con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        return con;
    }

    public static synchronized DBManager getInstance() {
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();

        try (Connection connection = getConnection();
             Statement st = connection.createStatement();
             ResultSet rs = st.executeQuery(SELECT_ALL_USERS)) {

            while (rs.next()) {
                users.add(mapUser(rs));
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("There are not users", e);
        }
    }

    public boolean insertUser(User user) throws DBException {
        try (Connection con = getConnection();
             PreparedStatement st = con.prepareStatement(INSERT_USER)) {
            st.setString(1, user.getLogin());
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot add user", e);
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        Connection con = null;
        try {
            con = getConnection(false);
            try (PreparedStatement st = con.prepareStatement(DELETE_USERS_FROM_USER_TEAM)) {
                for (User user : users) {
                    st.setInt(1, user.getId());
                    st.executeUpdate();
                }
            }
            try (PreparedStatement st2 = con.prepareStatement(DELETE_USERS)) {
                for (User user : users) {
                    st2.setInt(1, user.getId());
                    st2.executeUpdate();
                }
            }
            con.commit();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            rollback(con, e);
            throw new DBException("Cannot delete users", e);
        } finally {
            close(con);
        }
    }

    public User getUser(String login) throws DBException {
        try (Connection con = getConnection();
             PreparedStatement st = con.prepareStatement(SELECT_USER_BY_LOGIN)) {
            st.setString(1, login);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) return mapUser(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("There is no user with such login", e);
        }
        return null;
    }

    public Team getTeam(String name) throws DBException {
        try (Connection con = getConnection();
             PreparedStatement st = con.prepareStatement(SELECT_TEAM_BY_NAME)) {
            st.setString(1, name);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) return mapTeam(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("There is no team with such name", e);
        }
        return null;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();

        try (Connection connection = getConnection();
             Statement st = connection.createStatement();
             ResultSet rs = st.executeQuery(SELECT_ALL_TEAMS)) {

            while (rs.next()) {
                teams.add(mapTeam(rs));
            }
            return teams;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("There are not teams", e);
        }
    }

    public boolean insertTeam(Team team) throws DBException {
        try (Connection con = getConnection();
             PreparedStatement st = con.prepareStatement(INSERT_TEAM)) {
            st.setString(1, team.getName());
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot add team", e);
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection con = null;
        try {
            con = getConnection(false);
            try (PreparedStatement stUser = con.prepareStatement(SELECT_USER_BY_LOGIN)) {
                stUser.setString(1, user.getLogin());
                ResultSet rs = stUser.executeQuery();
                if (rs.next()) user.setId(rs.getInt(1));
                rs.close();
            }

            try (PreparedStatement st = con.prepareStatement(INSERT_USER_TEAM)) {
                for (int i = 0; i < teams.length; i++) {
                    st.setInt(1, user.getId());

                    try (PreparedStatement stTeam = con.prepareStatement(SELECT_TEAM_BY_NAME)) {
                        stTeam.setString(1, teams[i].getName());
                        ResultSet rs = stTeam.executeQuery();
                        if (rs.next()) teams[i].setId(rs.getInt(1));
                        rs.close();
                    }

                    st.setInt(2, teams[i].getId());
                    st.executeUpdate();
                }
                con.commit();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            rollback(con, e);
            throw new DBException("Cannot set team for user", e);
        } finally {
            close(con);
        }
        return false;
    }

    private void close(Connection con) {
        if (con != null)
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
    }

    private void rollback(Connection con, Exception e) throws DBException {
        if (con != null) try {
            con.rollback();
        } catch (SQLException ex) {
            ex.addSuppressed(e);
            ex.printStackTrace();
            throw new DBException("Cannot rollback", ex);
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();

        try (Connection con = getConnection();
             PreparedStatement st = con.prepareStatement(SELECT_TEAMS_BY_USER)) {
            st.setInt(1, user.getId());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                teams.add(mapTeam(rs));
            }
            rs.close();
            return teams;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("There are not teams", e);
        }
    }

    public boolean deleteTeam(Team team) throws DBException {
        Connection con = null;
        try {
            con = getConnection(false);
            try (PreparedStatement st = con.prepareStatement(DELETE_TEAM_FROM_USER_TEAM)) {
                st.setInt(1, team.getId());
                st.executeUpdate();
            }
            try (PreparedStatement st2 = con.prepareStatement(DELETE_TEAM)) {
                st2.setInt(1, team.getId());
                st2.executeUpdate();
            }
            con.commit();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            rollback(con, e);
            throw new DBException("Cannot delete team", e);
        } finally {
            close(con);
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection con = getConnection();
             PreparedStatement st = con.prepareStatement(UPDATE_TEAM)) {
            st.setString(1, team.getName());
            st.setInt(2, team.getId());
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot update team", e);
        }
        return false;
    }

    private User mapUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setLogin(rs.getString("login"));
        return user;
    }

    private Team mapTeam(ResultSet rs) throws SQLException {
        Team team = new Team();
        team.setId(rs.getInt("id"));
        team.setName(rs.getString("name"));
        return team;
    }
}
